FROM php:7.3.28-fpm
ARG SSH_PRIVATE_KEY
WORKDIR /var/www/html/api.gravitas.money

COPY --from=composer:1.10.10 /usr/bin/composer /usr/local/bin/composer
RUN apt-get update \
  && apt-get install -y \
    iputils-ping \
    libmagickwand-dev \
    libmemcached-dev \
    librabbitmq-dev \
    libmcrypt-dev \
    libxslt1-dev \
    libtidy-dev \
    libxml2-dev \
    libbz2-dev \
    libicu-dev \
    libpng-dev \
    libzip-dev \
    zlib1g-dev \
    vim \
    git \
  && docker-php-ext-install \
    pdo_mysql \
    calendar \
    gettext \
    sockets \
    sysvmsg \
    sysvsem \
    sysvshm \
    bcmath \
    mysqli \
    pcntl \
    shmop \
    exif \
    intl \
    soap \
    tidy \
    bz2 \
    xsl \
    zip \
    gd \
  && pecl install \
    memcached \
    igbinary \
    imagick \
    mongodb \
    msgpack \
    mcrypt \
    amqp \
  && docker-php-ext-enable \
    memcached \
    igbinary \
    imagick \
    mongodb \
    msgpack \
    mcrypt \
    amqp

RUN curl -LO https://deployer.org/deployer.phar &&\
    mv deployer.phar /usr/local/bin/dep &&\
    chmod +x /usr/local/bin/dep

RUN set -x
RUN mkdir /root/.ssh/ &&\
    echo "${SSH_PRIVATE_KEY}" > /root/.ssh/id_rsa &&\
    chmod 600 /root/.ssh/id_rsa &&\
    ssh-keyscan -t rsa,dsa bitbucket.org >> /root/.ssh/known_hosts
