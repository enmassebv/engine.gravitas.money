<?php
namespace Deployer;

require 'recipe/common.php';

set('bin_php', '/usr/local/php73/bin/php');
set(
    'bin/composer', function () {
    if (commandExist('composer')) {
        $composer = locateBinaryPath('composer');
    }
    if (empty($composer)) {
        run("cd {{release_path}} && curl -sS https://getcomposer.org/installer | {{bin/php}}");
        $composer = '{{release_path}}/composer.phar';
    }
    return '{{bin_php}} -d memory_limit=-1 ' . $composer;
}
);

// Project name
set('application', 'api.gravitas.money');

// Project repository
set('repository', 'git@gravitas.money:enmassebv/engine.gravitas.money.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 

// Shared files/dirs between deploys 
set('shared_files', ['.env']);
set('shared_dirs', ['var/log']);

// Writable dirs by web server
set('writable_dirs', ['var/cache', 'var/log']);
set('allow_anonymous_stats', false);

// Hosts
host('api.gravitas.money')
    ->stage('production')
    ->user('root')
    ->identityFile('~/.ssh/id_rsa')
    ->set('branch', 'develop')
    ->set('environment', 'prod')
    ->set('deploy_path', '/home/gravitas/domains/gravitas.money/api');


// Tasks
desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');


// Change permissions on the web server
task(
    'deploy:set_permissions', function () {
//    run('sudo chgrp -R apache {{deploy_path}}/current/var');
//    run('sudo chmod -R ug+rw {{deploy_path}}/current/var');
    run('sudo  chown gravitas:apache {{deploy_path}}/current/var -R');
    run('sudo  chown gravitas:apache {{deploy_path}}/current/var/log -R');

}
);

// Clear filesystem cache
task(
    'deploy:clear_filesystem_cache', function () {
    run('sudo rm -rf {{deploy_path}}/current/var/cache/prod/');
}
);

after('deploy', 'deploy:clear_filesystem_cache');
after('deploy:symlink', 'deploy:set_permissions');
