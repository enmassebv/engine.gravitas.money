<?php
declare(strict_types=1);

namespace App\Application\DataFixtures;

use App\Domain\Entity\Portfolio;
use App\Domain\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PortfolioFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $i = 1;
        foreach ($manager->getRepository(User::class)->findAll() as $user) {
            $portfolio = new Portfolio();
            $portfolio->setName('Portfolio '.$i);
            $portfolio->setUser($user);
            $manager->persist($portfolio);
            $manager->flush();
            $i++;
        }
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
