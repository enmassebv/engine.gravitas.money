<?php
declare(strict_types=1);

namespace App\Application\DataFixtures;

use App\Domain\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 5; ++$i) {
            $user = new User();
            $user->setFirstName('User '.$i);
            $user->setLastName('Name');
            $user->setEmail('user'.$i.'@gravitas.money');
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                'user'
            ));
            $user->setIsVerified(true);
            $user->setActive(true);
            $manager->persist($user);
            $manager->flush();
            $manager->persist($user);
            $manager->flush();
        }
    }
}
