<?php
declare(strict_types=1);

namespace App\Application\DataFixtures;

use App\Domain\Entity\Transaction;
use App\Domain\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TransactionFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        foreach ($manager->getRepository(User::class)->findAll() as $user) {
            $amountOfTransactions = rand(0, 15);
            for ($i = 0; $i < $amountOfTransactions; $i++) {
                $transaction = new Transaction();
                $transaction->setAmount(rand(0, 1000000));
                $transaction->setUser($user);
                $transaction->setType($this->getRandomType());
                $manager->persist($transaction);
                $manager->flush();
            }
        }
    }

    private function getRandomType()
    {
        $types = get_constants(\App\Application\Constants\Transaction::class, 'TYPE');
        $index = array_rand($types);

        return $types[$index];
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
