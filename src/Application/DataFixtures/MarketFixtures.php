<?php
declare(strict_types=1);

namespace App\Application\DataFixtures;

use App\Domain\Entity\Market;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MarketFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 20; ++$i) {
            $market = new Market();
            $market->setName('Market ' . $i);
            $market->setSymbol('Market_' . $i);
            $market->setPrice(rand(10, 100) * 1.0);
            $market->setLast24H(rand(10, 100));
            $manager->persist($market);
            $manager->flush();
        }
    }

}
