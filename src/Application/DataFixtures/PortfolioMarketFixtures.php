<?php
declare(strict_types=1);

namespace App\Application\DataFixtures;

use App\Domain\Entity\Market;
use App\Domain\Entity\Portfolio;
use App\Domain\Entity\PortfolioMarket;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PortfolioMarketFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $portfolios = $manager->getRepository(Portfolio::class)->findAll();
        $markets = $manager->getRepository(Market::class)->findAll();
        foreach ($portfolios as $portfolio) {
            foreach ($markets as $market) {
                $portfolioMarket = new PortfolioMarket();
                $portfolioMarket->setPortfolio($portfolio);
                $portfolioMarket->setUser($portfolio->getUser());
                $portfolioMarket->setMarket($market);
                $portfolioMarket->setSharePercentage(rand(10, 100));
                $portfolioMarket->setQuantity(rand(10, 100));
                $portfolioMarket->setNotifications(rand(0, 1) === 1 ? true : false);
                $manager->persist($portfolioMarket);
                $manager->flush();
            }
        }
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            PortfolioFixtures::class,
            MarketFixtures::class,
        ];
    }
}
