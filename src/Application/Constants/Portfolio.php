<?php

namespace App\Application\Constants;

class Portfolio
{
	const SORT_BY_MARKET_NAME = 'name';
	const SORT_BY_MARKET_SYMBOL = 'symbol';
    const SORT_BY_MARKET_CHANGE = 'change';
    const SORT_BY_LAST_24_HOURS = 'last24H';
    const SORT_BY_SHARE_PERCENTAGE = 'sharePercentage';
}
