<?php

namespace App\Application\Constants;

class Transaction
{
	const TYPE_DEPOSIT = 'deposit';
	const TYPE_WITHDRAW = 'withdraw';
	const TYPE_SELL = 'sell';
	const TYPE_BUY = 'buy';
}
