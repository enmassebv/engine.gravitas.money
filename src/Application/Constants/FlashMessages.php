<?php

namespace App\Application\Constants;

class FlashMessages
{
	const FLASHMESSAGE_TYPE_SUCCESS = 'success';
	const FLASHMESSAGE_TYPE_NOTICE = 'notice';
	const FLASHMESSAGE_TYPE_ERROR = 'error';
}
