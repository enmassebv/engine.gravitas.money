<?php

namespace App\Application\Controller\API;

use Overblog\GraphQLBundle\Controller\GraphController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
/**
 * Class GraphQLController
 *
 * Override because of CORS
 *
 * @package App\Application\Controller\API
 */
class GraphQLController extends GraphController
{
    /**
     * @param Request $request
     * @param null $schemaName
     * @return JsonResponse
     */
    public function endpointAction(Request $request, $schemaName = null)
    {
        if ($request->getMethod() === 'OPTIONS') {
            return $this->sendPreflightResponse();
        }
        $response = parent::endpointAction($request, $schemaName);
        return $response;
    }
    /**
     * @param Request $request
     * @param null $schemaName
     * @return JsonResponse
     */
    public function batchEndpointAction(Request $request, $schemaName = null)
    {
        if ($request->getMethod() === 'OPTIONS') {
            return $this->sendPreflightResponse();
        }


        $response = parent::batchEndpointAction($request, $schemaName);
        return $response;
    }
    /**
     * @return JsonResponse
     */
    private function sendPreflightResponse()
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, HEAD, PUT, DELETE');
        $response->headers->set('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, X-Requested-With, authorization');
        return $response;
    }
}
