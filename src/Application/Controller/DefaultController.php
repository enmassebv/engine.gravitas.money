<?php

namespace App\Application\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 *
 * @package App\Application\Controller
 */
class DefaultController
{
    /**
     * @Route ("/", name="homepage")
     *
     */
    public function indexAction(): Response
    {
        return new Response('hello moon');
    }
}
