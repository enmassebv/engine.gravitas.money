<?php

namespace App\Application\Command\Market;

use App\Domain\Entity\Market;
use App\Domain\Repository\MarketRepository;
use App\Infrastructure\GraphQL\Repository\MarketRepository as InfraMarketRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SyncMarketsCommand extends Command
{
    protected static $defaultName = 'app:sync-markets';
    protected static $defaultDescription = 'Sync markets';

    protected $marketRepository;
    protected $infraMarketRepository;
    protected $io;
    protected $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager,
        MarketRepository $marketRepository,
        InfraMarketRepository $infraMarketRepository
    ) {
        $this->entityManager = $entityManager;
        $this->marketRepository = $marketRepository;
        $this->infraMarketRepository = $infraMarketRepository;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io = new SymfonyStyle($input, $output);
        $this->io->info('Start sync markets');

        $markets = $this->infraMarketRepository->queryAll();
        $this->addMarkets($markets);
        $this->io->success('Done');

        return Command::SUCCESS;
    }

    private function addMarkets(array $markets): void
    {
        foreach ($markets as $market) {
            $marketExtRef = $market['id'];
            $marketName = $market['name'];
            $marketSymbol = $market['symbol'];
            $marketPrice = $market['price'];

            $this->io->info(sprintf('Looking for market with extRef %s (name: %s)...', $marketExtRef, $marketName));

            // Check if market exits
            $marketEntity = $this->marketRepository->findOneByExtRef($marketExtRef);

            if (!$marketEntity) {
                $this->io->info(sprintf('Market with extRef %s (name: %s) not found...', $marketExtRef, $marketName));
                $marketEntity = (new Market())
                    ->setExtRef($marketExtRef)
                    ->setName($marketName)
                    ->setSymbol($marketSymbol)
                    ->setPrice($marketPrice);
                $this->entityManager->persist($marketEntity);
                $this->io->info(sprintf('New market added with name: %s', $marketName));
            } else {
                $this->io->info(
                    sprintf(
                        'Market with extRef %s (name: %s) already exists, updating price',
                        $marketExtRef,
                        $marketName
                    )
                );
                $marketEntity->setPrice((float)$marketPrice);
            }

            $this->entityManager->flush();
        }
    }
}
