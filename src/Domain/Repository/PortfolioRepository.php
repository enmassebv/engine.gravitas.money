<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Portfolio;
use App\Domain\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;


class PortfolioRepository extends ServiceEntityRepository
{
    const DEFAULTNAME = 'Portfolio';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Portfolio::class);
    }

    public function getPortfoliosByUser(User $user)
    {
        $portfolios = $this->findBy(['user' => $user], ['createdAt' => 'ASC']);

        // Make sure our user has at least 1 default portfolio
        if (!$portfolios) {
            $portfolios = [$this->getNewDefaultPortfolioForUser($user)];
        }

        return $portfolios;
    }

    public function getActivePortfoliosByUser(User $user)
    {
        return $this->createQueryBuilder('p')
            ->where('p.user = :user')
            ->andWhere('p.active = 1')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }

    private function getNewDefaultPortfolioForUser(User $user): Portfolio
    {
        $portfolio = new Portfolio();
        $portfolio
            ->setName(self::DEFAULTNAME)
            ->setActive(true)
            ->setUser($user);
        $this->_em->persist($portfolio);
        $this->_em->flush();

        return $portfolio;
    }
}
