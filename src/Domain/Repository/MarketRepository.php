<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Market;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;


class MarketRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Market::class);
    }
    
    public function getBySearchTerm(string $searchTerm)
    {
        $searchTerm = trim($searchTerm);

        return $this->createQueryBuilder('m')
                    ->select('m.id, m.name, m.symbol, m.price')
                    ->andWhere('m.symbol = :termSymbol OR m.name LIKE :termName')
                    ->setParameter('termSymbol', '%'.$searchTerm. '%')
                    ->setParameter('termName', '%'.$searchTerm . '%')
                    ->setMaxResults(5)->getQuery()->getResult();
    }
}
