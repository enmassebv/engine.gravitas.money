<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Portfolio;
use App\Domain\Entity\Transaction;
use App\Domain\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;


class TransactionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Transaction::class);
    }

    private function getNewTransactionForUser(User $user): Portfolio
    {
        $transaction = new Transaction();
        $portfolio
            ->setName(self::DEFAULTNAME)
            ->setActive(true)
            ->setUser($user);
        $this->_em->persist($portfolio);
        $this->_em->flush();

        return $portfolio;
    }
}
