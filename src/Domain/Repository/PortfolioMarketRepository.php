<?php

namespace App\Domain\Repository;

use App\Domain\Entity\User;
use App\Domain\Entity\PortfolioMarket;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Application\Constants\Portfolio;


class PortfolioMarketRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PortfolioMarket::class);
    }

    public function getMarkets(array $portfolios, User $user,  string $search = null, string $sort = null, string $sortOrder = null, array $sortValue = null ){

        $qb = $this->createQueryBuilder('pm')
            ->join('pm.market', 'm')
            ->addSelect('m')
            ->where('pm.user = :user')
            ->andWhere('pm.portfolio in (:portfolios)')
            ->setParameter('user', $user)
            ->setParameter('portfolios', $portfolios);
        if($search){
            $qb->andWhere('m.symbol = :searchVal OR m.name LIKE :searchVal');
            $qb->setParameter('searchVal', '%' . $search . '%');

        }
        if(!empty($sortValue) && is_array($sortValue)){
            $sortTerms = explode(',', $sortValue[0]);
        }
        if($sort != ""){
            $sort;
        }elseif(!empty($sortTerms) && isset($sortTerms[0]) && isset($sortTerms[1])){
            $sort = $sortTerms[0];
            $sortOrder = $sortTerms[1];
        }else{
            if( Portfolio::SORT_BY_MARKET_NAME == 'name' ){
                $sort = 'name';
            }elseif( Portfolio::SORT_BY_MARKET_SYMBOL == 'symbol' ){
                $sort = 'symbol';
            }elseif( Portfolio::SORT_BY_MARKET_CHANGE == 'change' ){
                $sort = 'change';
            }elseif( Portfolio::SORT_BY_LAST_24_HOURS == 'last24H' ){
                $sort = 'last24H';
            }elseif( Portfolio::SORT_BY_SHARE_PERCENTAGE == 'sharePercentage' ){
                $sort = 'sharePercentage';
            }
            $sortOrder = ($sortOrder == "ASC") ? 'ASC' : 'DESC';
        }
        
        $mapping = [
            Portfolio::SORT_BY_MARKET_NAME => 'm.name',
            Portfolio::SORT_BY_MARKET_SYMBOL => 'm.symbol',
            Portfolio::SORT_BY_MARKET_CHANGE => 'm.totalValue',
            Portfolio::SORT_BY_LAST_24_HOURS => 'm.last24H',
            Portfolio::SORT_BY_SHARE_PERCENTAGE => 'm.sharePercentage'
        ];
        
        switch ($sort) {
            case $sort:
                $qb->addOrderBy($mapping[$sort], $sortOrder);
                break;
            default:
            $qb->addOrderBy('pm.createdAt', 'DESC');
                break;
        }

        return  $qb->getQuery()
                    ->getResult();
    }

    public function getTopGainMarkets(User $user ){

        $qb = $this->createQueryBuilder('pm')
            ->join('pm.market', 'm')
            ->addSelect('m')
            ->where('pm.user = :user')
            ->setParameter('user', $user)
            ->setMaxResults(5)
            ->addOrderBy('m.last24H', 'DESC');

        return  $qb->getQuery()
                    ->getResult();
    }

    public function getTopLooseMarkets(User $user ){

        $qb = $this->createQueryBuilder('pm')
            ->join('pm.market', 'm')
            ->addSelect('m')
            ->where('pm.user = :user')
            ->setParameter('user', $user)
            ->setMaxResults(5)
            ->addOrderBy('m.last24H', 'ASC');

        return  $qb->getQuery()
                    ->getResult();
    }

}
