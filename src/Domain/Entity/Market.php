<?php

namespace App\Domain\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Ramsey\Uuid\Uuid;
use Doctrine\ORM\Mapping\Index;

/**
 * Market
 *
 * @ORM\Table(name="market")
 * @ORM\Entity(repositoryClass="App\Domain\Repository\MarketRepository")
 *
 * @ORM\Table(name="market", indexes={
 *     @Index(name="extRef", columns={"extRef"}),
 * })
 *
 * @ORM\HasLifecycleCallbacks()
 */
class Market
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(nullable=true, unique=false))
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(length=20, nullable=false)
     * @Assert\NotBlank()
     */
    private $symbol;

    /**
     * @ORM\Column(type="float", nullable=true)
     *
     */
    private $price;

    /**
     * @ORM\Column(type="float", nullable=true)
     *
     */
    private $last24H;

    /**
     * @var \DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="extRef", type="string", nullable=true)
     */
    private $extRef;

    public function getId(): int
    {
        return $this->id;
    }

    public function getExtRef(): string
    {
        return $this->extRef;
    }

    public function setExtRef(string $extRef): self
    {
        $this->extRef = $extRef;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function fillExtRef(): void
    {
        $this->setExtRef(Uuid::uuid4()->toString());
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setSymbol(string $symbol): self
    {
        $this->symbol = $symbol;
        return $this;
    }

    public function getSymbol(): string
    {
        return $this->symbol;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;
        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setLast24H(float $last24H): self
    {
        $this->last24H = $last24H;

        return $this;
    }

    public function getLast24H(): ?float
    {
        return $this->last24H;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}
