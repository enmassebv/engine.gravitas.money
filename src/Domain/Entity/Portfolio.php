<?php

namespace App\Domain\Entity;

use App\Domain\Repository\PortfolioRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Gedmo\Mapping\Annotation as Gedmo;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=PortfolioRepository::class)
 * @ORM\Table(name="portfolio", indexes={
 *     @Index(name="extRef", columns={"extRef"}),
 * })
 * @ORM\HasLifecycleCallbacks()
 */
class Portfolio
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="extRef", type="string", nullable=true)
     */
    private $extRef;

    /**
     * @var string
     *
     * @ORM\Column(nullable=true, unique=false))
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\User", fetch="EAGER", inversedBy="portfolios")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Entity\PortfolioMarket", mappedBy="portfolio", orphanRemoval=true, fetch="EXTRA_LAZY")
     */
    private $marketRelations = [];

    /**
     * @ORM\Column(type="boolean")
     */
    private $active = true;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $storeSortValue;

    /**
     * @var \DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExtRef(): string
    {
        return $this->extRef;
    }

    public function setExtRef(string $extRef): self
    {
        $this->extRef = $extRef;

        return $this;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @ORM\PrePersist()
     */
    public function fillExtRef(): void
    {
        $this->setExtRef(Uuid::uuid4()->toString());
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug($slug): self
    {
        $this->slug = $slug;
        return $this;
    }

    public function hasNotifications(): bool
    {
        return $this->notifications;
    }

    public function setNotifications(bool $notifications): self
    {
        $this->notifications = $notifications;
        return $this;
    }

    public function getMarketRelations()
    {
        return $this->marketRelations;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;
        return $this;
    }

    public function getstoreSortValue(): ?string
    {
        return $this->storeSortValue;
    }

    public function setstoreSortValue(?string $storeSortValue): self
    {
        $this->storeSortValue = $storeSortValue;

        return $this;
    }

}
