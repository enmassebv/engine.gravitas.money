<?php

namespace App\Domain\Entity;

use _HumbugBoxe5640220fe34\Symfony\Component\Console\Exception\InvalidArgumentException;
use App\Domain\Repository\TransactionRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Gedmo\Mapping\Annotation as Gedmo;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=TransactionRepository::class)
 * @ORM\Table(name="transaction", indexes={
 *     @Index(name="extRef", columns={"extRef"}),
 * })
 * @ORM\HasLifecycleCallbacks()
 */
class Transaction
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="extRef", type="string", nullable=true)
     */
    private $extRef;

    /**
     * @var string
     *
     * @ORM\Column(nullable=true, unique=false))
     * @Assert\NotBlank()
     */
    private $type;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $amount;

    /**
     * @var \App\Domain\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\User", fetch="EAGER")
     */
    private $user;

    /**
     * @var \DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setType(string $type): self
    {
        if(!has_constant(\App\Application\Constants\Transaction::class, 'TYPE', $type)) {
            throw new InvalidArgumentException('Unknown type!');
        }

        $this->type = $type;
        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function getExtRef(): string
    {
        return $this->extRef;
    }

    public function setExtRef(string $extRef): self
    {
        $this->extRef = $extRef;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function fillExtRef(): void
    {
        $this->setExtRef(Uuid::uuid4()->toString());
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;
        return $this;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

}
