<?php

namespace App\Domain\Entity;

use App\Domain\Repository\UserRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Gedmo\Mapping\Annotation as Gedmo;
use Ramsey\Uuid\Uuid;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\User\UserInterface;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="user", indexes={
 *     @Index(name="extRef", columns={"extRef"}),
 * })
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, nullable=true)
     */
    protected $firstName;

    /**
     * @ORM\Column(type="string", length=180, nullable=true)
     */
    protected $lastName;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string")
     */
    private $roles = '{}';

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $avatar;

    /**
     * @Vich\UploadableField(mapping="avatarImage", fileNameProperty="avatar")
     * @var File
     */
    private $avatarImage;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active = true;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Entity\Portfolio", mappedBy="user")
     */
    private $portfolios;

    /**
     * @var \DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="extRef", type="string", nullable=true)
     */
    private $extRef;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->portfolios = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }


    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getExtRef(): string
    {
        return $this->extRef;
    }

    public function setExtRef(string $extRef): self
    {
        $this->extRef = $extRef;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function fillExtRef(): void
    {
        $this->setExtRef(Uuid::uuid4()->toString());
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = json_decode($this->roles, true);
        // Guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = json_encode($roles);

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getFullName()
    {
        $nameParts = [];
        if ($firstName = $this->getFirstName()) {
            $nameParts[] = $firstName;
        }
        if ($lastName = $this->getLastName()) {
            $nameParts[] = $lastName;
        }

        return $nameParts ? implode(' ', $nameParts) : $this->getUsername();
    }


    public function setAvatarImage(File $image = null)
    {
        $this->avatarImage = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
        return $this;
    }

    public function getAvatarImage(): ?File
    {
        return $this->avatarImage;
    }

    public function setAvatar(?string $image): self
    {
        $this->avatar = $image;
        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;
        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;
        return $this;
    }

    public function getPortfolios()
    {
        return $this->portfolios;
    }

    public function setPortfolios($portfolios): self
    {
        $this->portfolios = $portfolios;

        return $this;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * Needed because this object is also used for authentication
     * Symfony will serialize this object, but we cant serialize a fileobject
     * for example
     *
     * @return string
     */
    public function __sleep(): array
    {
        $allProperties = array_keys(get_object_vars($this));
        $filtered = array_diff($allProperties, ['avatarImage']);

        return $filtered;
    }

}
