<?php

namespace App\Domain\Entity;

use App\Domain\Repository\PortfolioMarketRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Table(name="portfolio_x_market")
 * @ORM\Entity(repositoryClass=PortfolioMarketRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class PortfolioMarket
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="extRef", type="string", nullable=true)
     */
    private $extRef;

    /**
     * @var \App\Domain\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\User", fetch="EAGER")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Portfolio", fetch="EAGER", inversedBy="marketRelations")
     */
    private $portfolio;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Market", fetch="EAGER")
     * @ORM\JoinColumn(referencedColumnName="id",onDelete="CASCADE")
     */
    private $market;

    /**
     * @ORM\Column(type="float", nullable=true)
     *
     */
    private $sharePercentage;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $quantity;

    /**
     * @ORM\Column(type="boolean")
     */
    private $notifications = false;

    /**
     * @var \DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExtRef(): string
    {
        return $this->extRef;
    }

    public function setExtRef(string $extRef): self
    {
        $this->extRef = $extRef;
        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function fillExtRef(): void
    {
        $this->setExtRef(Uuid::uuid4()->toString());
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;
        return $this;
    }

    public function getMarket(): ?Market
    {
        return $this->market;
    }

    public function getPortfolio(): ?Portfolio
    {
        return $this->portfolio;
    }

    public function setPortfolio(Portfolio $portfolio): self
    {
        $this->portfolio = $portfolio;
        return $this;
    }

    public function setMarket(Market $market): self
    {
        $this->market = $market;
        return $this;
    }

    public function setSharePercentage(float $sharePercentage): self
    {
        $this->sharePercentage = $sharePercentage;
        return $this;
    }

    public function getSharePercentage(): ?float
    {
        return $this->sharePercentage;
    }

    public function setQuantity(float $quantity): self
    {
        $this->quantity = $quantity;
        return $this;
    }

    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    public function hasNotifications(): bool
    {
        return $this->notifications;
    }

    public function setNotifications(bool $notifications): self
    {
        $this->notifications = $notifications;
        return $this;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}
