<?php

namespace App\Domain\ValueObject;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Emailaddress
 * @package App\Domain\ValueObject
 */
class Emailaddress
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    private $value;

    /**
     * Hostname constructor.
     * @param $value
     */
    public function __construct($value)
    {
        // Always convert to lowercase
        $value = strtolower($value);

        // Validate
        if (!$this->isValidEmailAddress($value)) {
            throw new \InvalidArgumentException(sprintf('Invalid emailaddress "%s" given!', $value));
        }

        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param $value
     * @return bool
     */
    private function isValidEmailAddress($value)
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getValue();
    }

}