<?php

/**
 * Extract a value from an item (array) that sits in a collection (array) of items
 *
 * @param iterable $input
 * @param $key
 * @param bool $recursive
 * @param array $result , dont supply yourself!
 *
 * @return array
 */
function pluck(iterable $input, $key, $recursive = false, array &$result = [])
{
    foreach ($input as $item) {

        // Skip empty
        if (!is_array($item) && !is_object($item)) {
            continue;
        }

        // Handle the item as an array
        if (is_array($item) && array_key_exists($key, $item)) {
            $result[] = $item[$key];
        } elseif (is_object($item)) {
            $reflected = new \ReflectionClass($item);
            if ($reflected->hasProperty($key)) {
                $property = $reflected->getProperty($key);
                if (!$property->isPublic()) {
                    $property->setAccessible(true);
                }
                $result[] = $property->getValue($item);
            } elseif (in_array(\Doctrine\ORM\Proxy\Proxy::class, $reflected->getInterfaceNames())) {
                // Doctrine entity proxies don't expose the property as the data isn't loaded yet, so we
                //   need to call the getter here...
                $getter = 'get'.$key;
                if ($reflected->hasMethod($getter)) {
                    $result[] = $reflected->getMethod($getter)->invoke($item);
                }
            }
        }

        if ($recursive && is_array($item)) {
            pluck($item, $key, $recursive, $result);
        }
    }

    return $result;
}

/**
 * @param callable $callback
 * @param array $input
 * @param array=[] $callbackArguments
 * @return array
 */
function array_map_assoc(callable $callback, array $input, array $callbackArguments = []): array
{
    $values = [];
    foreach ($input as $key => $value) {
        $values[$key] = call_user_func($callback, $value, ...$callbackArguments);
    }

    return $values;
}

/**
 * @param $input
 * @return array
 */
function array_flatten($input)
{
    $result = [];
    foreach ($input as $key => $value) {
        if (is_array($value)) {
            $result = array_merge($result, array_flatten($value));
        } else {
            $result[$key] = $value;
        }
    }
    return $result;

}

/**
 * @param array $input
 * @param $key
 * @return array
 */
function setKeysByPlucking(array &$input, $key)
{
    $keys = pluck($input, $key);
    $input = array_combine($keys, $input);
    return $input;
}

/**
 * Can be very helpfull, for example when you have a block of HTML with multiple <p></p> tags and you want to
 * place a blockquote after the first </p> tag
 *
 * @param string $search
 * @param string $replace
 * @param string $subject
 * @return mixed
 */
function replaceFirstOccurence(string $search, string $replace, string $subject)
{
    // Render quote after the first paragraph
    $from = '/' . preg_quote($search, '/') . '/';
    return preg_replace($from, $replace, $subject, 1);
}

/**
 * Replace the last occurence in a string
 *
 * @param string $search
 * @param string $replace
 * @param string $subject
 * @return mixed|string
 */
function replaceLastOccurence(string $search, string $replace, string $subject)
{
    $pos = strrpos($subject, $search);
    if ($pos !== false) {
        $subject = substr_replace($subject, $replace, $pos, strlen($search));
    }

    return $subject;
}

/**
 * @param $needle
 * @param $haystack
 *
 * @return bool
 */
function strContains($needle, $haystack)
{
    return strpos($haystack, $needle) !== false;
}

/**
 * @param $destination
 * @param $sourceObject
 * @return mixed
 */
function inheritValues($destination, $sourceObject)
{
    if (is_string($destination)) {
        $destination = new $destination();
    }
    $sourceReflection = new \ReflectionObject($sourceObject);
    $destinationReflection = new \ReflectionObject($destination);
    $sourceProperties = $sourceReflection->getProperties();
    foreach ($sourceProperties as $sourceProperty) {
        $sourceProperty->setAccessible(true);
        $name = $sourceProperty->getName();
        $value = $sourceProperty->getValue($sourceObject);
        if ($destinationReflection->hasProperty($name)) {
            $propDest = $destinationReflection->getProperty($name);
            $propDest->setAccessible(true);
            $propDest->setValue($destination, $value);
        } else {
            $destination->$name = $value;
        }
    }

    return $destination;
}

/**
 * @param array $data
 * @return array
 */
function removeNullValuesFromArray(array $data)
{
    foreach ($data as $key => $value) {
        if ($value === null) {
            unset($data[$key]);
        }
    }

    return $data;
}

/**
 * @param array $data
 * @param $value
 * @return array
 */
function removeArrayValue(array $data, $value)
{
    if (($key = array_search($value, $data)) !== false) {
        unset($data[$key]);
    }

    return $data;
}

/**
 * @param string $class
 * @param string $filter
 * @param $value
 * @return bool
 * @throws ReflectionException
 */
function has_constant(string $class, string $filter, $value)
{
    $reflection = new \ReflectionClass($class);
    foreach ($reflection->getConstants() as $constantName => $constantValue) {
        if (strpos($constantName, $filter) === 0 && $constantValue === $value) {
            return true;
        }
    }
    return false;
}

/**
 * Get a class' constants beginning with $part, e.g. <code>get_constants(\AWQL\QueryBuilder::class, 'DURING_DATERANGE_LITERAL_')</code>
 *
 * @param string $class
 * @param string='' $part
 */
function get_constants(string $class, string $part = '')
{
    $reflection = new \ReflectionClass($class);
    $constants = [];

    foreach ($reflection->getConstants() as $constantName => $constantValue) {
        if ($part === '' || strpos($constantName, $part) === 0) {
            $constants[$constantName] = $constantValue;
        }
    }

    return $constants;
}
