<?php

namespace App\Infrastructure\GraphQL\Mutator;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Overblog\GraphQLBundle\Definition\Argument;

/**
 * Class User
 *
 * @package App\Infrastructure\GraphQL\Mutator
 *
 * mutation user {
 *      createUser(input: {
 *          xxx: "xxx",
 *      }) {
 *          id
 *      }
 * }
 */
class User implements CreateInterface
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(
        EntityManager $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Argument $input
     *
     * @return string[]
     *
     * @psalm-return array{url: string}
     */
    public function create(Argument $argument)
    {
        $input = $argument->offsetGet('input');

        $user = (new \App\Domain\Entity\User())
            ->setEmail($input['email'])
            ->setFirstName($input['firstName'])
            ->setLastName($input['lastName'])
            ->setPassword(
                md5(random_bytes(10))
            ); // just a random password, unhashed altough in real life this shoudl be hashed

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $qb = $this->entityManager->getRepository(\App\Domain\Entity\User::class)->createQueryBuilder('u')
            ->where('u.id = :id')
            ->setParameter('id', $user->getId())
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }
}
