<?php

declare(strict_types=1);

namespace App\Infrastructure\GraphQL\Resolver;

use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Overblog\GraphQLBundle\Definition\Argument;

/**
 * Class User
 *
 * @package App\Infrastructure\GraphQL\Resolver
 *
 *query test{
 *  user(id:"a13b19a6-08dd-433d-b9dc-9e4531d664b7") {
 *      id
 *      name
 *  }
 *}
 */
class User extends Base implements SingleInterface, MultipleInterface
{
    public function fetch(Argument $input, array $requestedFields)
    {
        $userId = $input->offsetGet('id');
        $qb = $this->getQueryBuilder($requestedFields)
            ->where('u.extRef = :userId')
            ->setParameter('userId', $userId)
            ->setMaxResults(1);
        return $qb->getQuery()->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

    public function fetchAll(Argument $input, array $requestedFields): array
    {
        $qb = $this->getQueryBuilder($requestedFields);
        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    private function getQueryBuilder(array $requestedFields): QueryBuilder
    {
        $repo = $this->entityManager->getRepository(\App\Domain\Entity\User::class);
        $qb = $repo->createQueryBuilder('u')
            ->select('u.extRef AS id');
        // Extend the query with the additional requested fields
        $validFields = ['firstName', 'email', 'lastName'];
        foreach (array_keys($requestedFields) as $field) {
            if (in_array($field, $validFields)) {
                $qb->addSelect('u.' . $field);
            }
        }
        return $qb;
    }

    /**
     * @param array $values
     * @return array
     */
    public function fetchByRelation($id, array $requestedFields)
    {
        $argument = new Argument(['id' => $id]);
        return $this->fetch($argument, $requestedFields);
    }
}
