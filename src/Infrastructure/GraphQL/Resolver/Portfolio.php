<?php

declare(strict_types=1);

namespace App\Infrastructure\GraphQL\Resolver;

use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Overblog\GraphQLBundle\Definition\Argument;

/**
 * Class Portfolio
 *
 * @package App\Infrastructure\GraphQL\Resolver
 *
 *query test{
 *  portfolio(id:"a13b19a6-08dd-433d-b9dc-9e4531d664b7") {
 *      id
 *      name
 *  }
 *}
 */
class Portfolio extends Base implements SingleInterface, MultipleInterface
{
    public function fetch(Argument $input, array $requestedFields)
    {
        $portfolioId = $input->offsetGet('id');
        $qb = $this->getQueryBuilder($requestedFields)
            ->where('p.extRef = :portfolioId')
            ->setParameter('portfolioId', $portfolioId)
            ->setMaxResults(1);
        return $qb->getQuery()->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

    public function fetchAll(Argument $input, array $requestedFields): array
    {
        $qb = $this->getQueryBuilder($requestedFields);
        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    private function getQueryBuilder(array $requestedFields): QueryBuilder
    {
        $repo = $this->entityManager->getRepository(\App\Domain\Entity\Portfolio::class);
        $qb = $repo->createQueryBuilder('p')
            ->select('p.extRef AS id');
        // Extend the query with the additional requested fields
        $validFields = ['name', 'slug'];
        foreach (array_keys($requestedFields) as $field) {
            if (in_array($field, $validFields)) {
                $qb->addSelect('p.' . $field);
            }
        }
        if (isset($requestedFields['user']))
        {
            $qb->leftJoin('p.user', 'u')
                ->addSelect('u.extRef AS user');
        }
        return $qb;
    }

    public function fetchByRelation($id, array $requestedFields)
    {
        $argument = new Argument(['id' => $id]);
        return $this->fetch($argument, $requestedFields);
    }

    public function fetchByUserRelation($id, array $requestedFields): array
    {
        $qb = $this->getQueryBuilder($requestedFields);

        if (!isset($requestedFields['user']))
        {
            $qb->leftJoin('p.user', 'u');
        }

        $qb->where('u.extRef = :user')
            ->setParameter('user', $id);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }
}
