<?php

namespace App\Infrastructure\GraphQL\Resolver;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Class Base
 *
 * @package App\Infrastructure\GraphQL\Resolver
 */
abstract class Base
{
    /**
     * @var
     */
    protected $entityManager;

    /**
     * Market constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
}
