<?php

namespace App\Infrastructure\GraphQL\Resolver;

use Overblog\GraphQLBundle\Definition\Argument;

/**
 * Interface SingleInterface
 *
 * @package App\Infrastructure\GraphQL\Resolver
 */
interface SingleInterface
{
    /**
     * @param Argument $input
     * @return array
     */
    public function fetch(Argument $input, array $requestedFields);
}
