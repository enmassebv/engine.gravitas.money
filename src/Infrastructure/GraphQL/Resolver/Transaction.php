<?php

declare(strict_types=1);

namespace App\Infrastructure\GraphQL\Resolver;

use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Overblog\GraphQLBundle\Definition\Argument;

/**
 * Class Transaction
 *
 * @package App\Infrastructure\GraphQL\Resolver
 *
 *}
 */
class Transaction extends Base implements SingleInterface, MultipleInterface
{
    public function fetch(Argument $input, array $requestedFields)
    {
        $portfolioId = $input->offsetGet('id');
        $qb = $this->getQueryBuilder($requestedFields)
            ->where('p.extRef = :transactionId')
            ->setParameter('transactionId', $portfolioId)
            ->setMaxResults(1);
        return $qb->getQuery()->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

    public function fetchAll(Argument $input, array $requestedFields): array
    {
        $qb = $this->getQueryBuilder($requestedFields);
        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    private function getQueryBuilder(array $requestedFields): QueryBuilder
    {
        $repo = $this->entityManager->getRepository(\App\Domain\Entity\Transaction::class);
        $qb = $repo->createQueryBuilder('t')
            ->select('t.extRef AS id');
        // Extend the query with the additional requested fields
        $validFields = ['type', 'amount'];
        foreach (array_keys($requestedFields) as $field) {
            if (in_array($field, $validFields)) {
                $qb->addSelect('t.' . $field);
            }
        }
        if (isset($requestedFields['user'])) {
            $qb->leftJoin('t.user', 'u')
                ->addSelect('u.extRef AS user');
        }
        return $qb;
    }

    public function fetchByRelation($id, array $requestedFields)
    {
        $argument = new Argument(['id' => $id]);
        return $this->fetch($argument, $requestedFields);
    }

    public function fetchByUserRelation($id, array $requestedFields): array
    {
        $qb = $this->getQueryBuilder($requestedFields);
        if (!isset($requestedFields['user'])) {
            $qb->leftJoin('t.user', 'u');
        }

        $qb->where('u.extRef = :user')
            ->setParameter('user', $id);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }
}
