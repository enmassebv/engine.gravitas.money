<?php

declare(strict_types=1);

namespace App\Infrastructure\GraphQL\Resolver;

use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Overblog\GraphQLBundle\Definition\Argument;

class PortfolioMarket extends Base implements SingleInterface
{
    public function fetch(Argument $input, array $requestedFields)
    {
        $portfolioMarketId = $input->offsetGet('id');
        $qb = $this->getQueryBuilder($requestedFields)
            ->where('pm.extRef = :portfolioMarketId')
            ->setParameter('portfolioMarketId', $portfolioMarketId)
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

    private function getQueryBuilder(array $requestedFields): QueryBuilder
    {
        $repo = $this->entityManager->getRepository(\App\Domain\Entity\PortfolioMarket::class);
        $qb = $repo->createQueryBuilder('pm')
            ->select('pm.extRef AS id');

        // Extend the query with the additional requested fields
        $validFields = ['quantity', 'sharePercentage'];
        foreach (array_keys($requestedFields) as $field) {
            if (in_array($field, $validFields)) {
                $qb->addSelect('pm.' . $field);
            }
        }

        // Relations
        if (isset($requestedFields['user'])) {
            $qb->leftJoin('pm.user', 'u')
                ->addSelect('u.extRef AS user');
        }
        if (isset($requestedFields['portfolio'])) {
            $qb->leftJoin('pm.portfolio', 'p')
                ->addSelect('p.extRef AS portfolio');
        }
        if (isset($requestedFields['market'])) {
            $qb->leftJoin('pm.market', 'm')
                ->addSelect('m.extRef AS market');
        }

        return $qb;
    }

    public function fetchByRelation($id, array $requestedFields)
    {
        $argument = new Argument(['id' => $id]);
        return $this->fetch($argument, $requestedFields);
    }

    public function fetchByPortfolioRelation($id, array $requestedFields): array
    {
        $qb = $this->getQueryBuilder($requestedFields);
        $qb->leftJoin('pm.portfolio', 'p');
        $qb->where('p.extRef = :portfolioExtRef')
            ->setParameter('portfolioExtRef', $id);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

}
