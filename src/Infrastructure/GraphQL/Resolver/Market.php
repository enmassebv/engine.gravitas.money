<?php

declare(strict_types=1);

namespace App\Infrastructure\GraphQL\Resolver;

use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Overblog\GraphQLBundle\Definition\Argument;

class Market extends Base implements SingleInterface, MultipleInterface
{

    public function fetch(Argument $input, array $requestedFields)
    {
        $marketId = $input->offsetGet('id');
        $qb = $this->getQueryBuilder($requestedFields)
            ->where('m.extRef = :marketId')
            ->setParameter('marketId', $marketId)
            ->setMaxResults(1);
        return $qb->getQuery()->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

    public function fetchAll(Argument $input, array $requestedFields)
    {
        $qb = $this->getQueryBuilder($requestedFields);
        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    private function getQueryBuilder(array $requestedFields): QueryBuilder
    {
        $repo = $this->entityManager->getRepository(\App\Domain\Entity\Market::class);
        $qb = $repo->createQueryBuilder('m')
            ->select('m.extRef AS id');
        $validFields = ['name', 'symbol', 'price', 'last24H'];
        foreach (array_keys($requestedFields) as $field) {
            if (in_array($field, $validFields)) {
                $qb->addSelect('m.' . $field);
            }
        }
        return $qb;
    }

    public function fetchByRelation($id, array $requestedFields)
    {
        $argument = new Argument(['id' => $id]);
        return $this->fetch($argument, $requestedFields);
    }

}
