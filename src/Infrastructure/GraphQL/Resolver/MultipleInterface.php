<?php

namespace App\Infrastructure\GraphQL\Resolver;

use Overblog\GraphQLBundle\Definition\Argument;

/**
 * Interface SingleInterface
 *
 * @package App\Infrastructure\GraphQL\Resolver
 */
interface MultipleInterface
{
    /**
     * @param Argument $input
     * @return array
     */
    public function fetchAll(Argument $input, array $requestedFields);
}
