<?php

namespace App\Infrastructure\GraphQL\Repository;

use App\Infrastructure\GraphQL\Client;

class MarketRepository
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function queryAll(): array
    {
        $headers[] = 'Content-Type: application/json';
        $query = '{"query":"query fetch {assets {token, name, symbol, prices{oraclePrice}}}"}';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://graph.mirror.finance/graphql');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        $result = json_decode($response, true);
        curl_close($ch);

        $assets = [];
        foreach ($result['data']['assets'] as $asset) {

            // Only m assets
            if (substr($asset['symbol'], 0, 1) !== 'm') {
                continue;
            }

            $assets[] = [
                'id'     => $asset['token'],
                'name'   => $asset['name'],
                'symbol' => $asset['symbol'],
                'price'  => $asset['prices']['oraclePrice'],
            ];
        }

        return $assets;
    }

}
